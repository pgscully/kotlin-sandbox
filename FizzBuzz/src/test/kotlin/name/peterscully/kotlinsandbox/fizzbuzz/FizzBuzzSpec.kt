package name.peterscully.kotlinsandbox.fizzbuzz

import com.google.common.truth.Truth.assertThat
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

object FizzBuzzSpec : Spek({

    given("an input of -5") {
        val input = -5

        on ("calculating FizzBuzz") {
            val result = FizzBuzz.fizzBuzz(input)

            it("should return an empty list") {
                val expected = emptyList<String>()
                assertThat(result).isEqualTo(expected)
            }
        }
    }

    given("an input of 0") {
        val input = 0

        on ("calculating FizzBuzz") {
            val result = FizzBuzz.fizzBuzz(input)

            it("should return an empty list") {
                val expected = emptyList<String>()
                assertThat(result).isEqualTo(expected)
            }
        }
    }

    given("an input of 1") {
        val input = 1

        on("calculating FizzBuzz") {
            val result = FizzBuzz.fizzBuzz(input)

            it("should give 1") {
                val expected = listOf("1")
                assertThat(result).isEqualTo(expected)
            }
        }
    }

    given("an input of 5") {
        val input = 5

        on("calculating FizzBuzz") {
            val result = FizzBuzz.fizzBuzz(input)

            it("should give 1, 2, Fizz, 4, Buzz") {
                val expected = listOf("1", "2", "Fizz", "4", "Buzz")
                assertThat(result).isEqualTo(expected)
            }
        }
    }

    given("an input of 15") {
        val input = 15

        on("calculating FizzBuzz") {
            val result = FizzBuzz.fizzBuzz(input)

            it("should give the correct result") {
                val expected = listOf("1", "2", "Fizz", "4", "Buzz",
                        "Fizz", "7", "8", "Fizz", "Buzz",
                        "11", "Fizz", "13", "14", "FizzBuzz")
                assertThat(result).isEqualTo(expected)
            }
        }
    }
})
