package name.peterscully.kotlinsandbox.fizzbuzz

object FizzBuzz {
    private const val fizzVal = 3
    private const val buzzVal = 5
    private const val fizzBuzzVal = fizzVal * buzzVal

    fun fizzBuzz(num: Int): List<String> {
        // TODO: check num >= 1
        return (1..num).map { it ->
            when {
                it % fizzBuzzVal == 0 -> "FizzBuzz"
                it % buzzVal == 0 -> "Buzz"
                it % fizzVal == 0 -> "Fizz"
                else -> it.toString()
            }
        }
    }
}
