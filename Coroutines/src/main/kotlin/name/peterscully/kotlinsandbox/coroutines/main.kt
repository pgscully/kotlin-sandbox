package name.peterscully.kotlinsandbox.coroutines

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Thread.sleep
import kotlin.system.measureTimeMillis

/*
 * Things to play with
 *
 * how to suspend an Expensive function
 *
 * yield and making a prime sieve
 *
 * Actors
 */

fun main(args: Array<String>) {
    println("Expensive squares without coroutines")
    val time1 = measureTimeMillis {
        val result = (1..10).map { Expensive.calculation(it) }
        result.map { print("$it ") }
    }
    println("\nTime1 = $time1")

    println()
    println("Expensive squares with coroutines launch")
    val time2 = measureTimeMillis {
        GlobalScope.launch {
//            val result = (1..10).map { Expensive.calculationAsync(it).await() }
//            result.map { print("$it ") }
        }
    }
    println("Time2 = $time2")
    sleep(2000)

    println()
    println("Channels test")
    GlobalScope.launch {
//        for (number in Channels.intChannel) {
//            println("Latest number is $number")
//        }
    }
    sleep(10000)
}
