package name.peterscully.kotlinsandbox.coroutines

import java.lang.Thread.sleep

object Expensive {

    private const val delay = 100L

    fun calculation(num: Int): Int {
        sleep(delay)
        return num * num
    }

//    fun calculationAsync(num: Int): Deferred<Int> {
//        return calculation(num = num)
//    }
}
