package name.peterscully.kotlinsandbox.movingaverage

import com.google.common.truth.Truth.assertThat
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

object MovingAverageSpec : Spek({

    given("A list of numbers") {
        val input = arrayListOf(1.0F, 2.0F, 3.0F, 4.0F, 5.0F)

        on("calculating a simple moving average") {
            val res = movingAverage(entries = input, window = 3, averageCalc = { mean() })

            it("should give the correct result") {
                assertThat(res).containsExactly(2.0F, 3.0F, 4.0F)
                        .inOrder()
            }
        }

        on("calculating a weighted moving average") {
            val res = movingAverage(entries = input, window = 3, averageCalc = { weightedMean() })

            it("should give the correct result") {
                assertThat(res).containsExactly(2.3333333F, 3.3333333F, 4.3333335F)
                        .inOrder()
            }
        }
    }
})
