package name.peterscully.kotlinsandbox.movingaverage

import com.google.common.truth.Truth.assertThat
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

object MeansSpec : Spek({

    given("a list of numbers") {
        val input = arrayListOf(1.0F, 2.0F, 3.0F, 4.0F, 5.0F)

        on("calculating it's simple mean") {
            val res = input.mean()

            it("should return the correct mean") {
                assertThat(res).isEqualTo(3.0F)
            }
        }

        on("calculating its weighted mean") {
            val res = input.weightedMean()

            it("should return the correct weighted mean") {
                assertThat(res).isEqualTo(3.6666667F) // use equal within acceptable range
            }
        }
    }
})
