package name.peterscully.kotlinsandbox.movingaverage

fun Iterable<Float>.mean(): Float {
    val sum: Float = this.sum()
    return sum / this.count()
}

fun sumTo(n: Int): Int = n * (n + 1) / 2

fun Iterable<Float>.weightedMean(): Float {
    val sum: Float = this
            .mapIndexed { index, t -> t * (index + 1) }
            .sum()
    return sum / sumTo(this.count())
}
