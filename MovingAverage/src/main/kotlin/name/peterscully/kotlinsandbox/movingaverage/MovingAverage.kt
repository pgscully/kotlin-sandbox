package name.peterscully.kotlinsandbox.movingaverage

fun movingAverage(entries: List<Float>, window: Int,
                  averageCalc: Iterable<Float>.() -> Float): List<Float> =
        entries.asSequence().windowed(size = window).map { it -> it.averageCalc() }.toList()
