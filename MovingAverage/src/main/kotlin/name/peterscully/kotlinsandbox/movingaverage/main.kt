package name.peterscully.kotlinsandbox.movingaverage

fun main(args: Array<String>) {
    println("Hello Moving Averages!")

    val input: List<Float> = arrayListOf(1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 25.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F, 1.0F,
            1.0F, 1.0F, 1.0F)

    val simpleAverage = movingAverage(entries = input, window = 5, averageCalc = { mean() })
    val weightedAverage = movingAverage(entries = input, window = 5, averageCalc = { weightedMean() })

    println("input    = $input")
    println("simple   = $simpleAverage")
    println("weighted = $weightedAverage")
}
